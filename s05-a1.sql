SELECT customerName FROM customers WHERE country = "Philippines";
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";
SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName, lastName, email FROM  employees WHERE lastName = "Patterson" AND firstName = "Steve";
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;
SELECT DISTINCT status FROM orders;
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");
SELECT e.firstName, e.lastName, o.city FROM employees AS e JOIN offices AS o ON e.officeCode = o.officeCode WHERE o.city = "Tokyo";
SELECT customerName FROM customers AS c JOIN employees AS e ON c.salesRepEmployeeNumber = e.employeeNumber WHERE e.firstName = "Leslie" AND e.lastName = "Thompson";

SELECT p.productName, c.customerName FROM customers AS c JOIN orders AS o ON c.customerNumber = o.customerNumber JOIN orderdetails AS od ON o.orderNumber = od.orderNumber JOIN products as p ON od.productCode = p.productCode WHERE c.customerName = "Baane Mini Imports"; 
SELECT e.firstName, e.lastName, c.customerName, o.country FROM payments AS p JOIN customers AS c ON p.customerNumber = c.customerNumber JOIN employees AS e ON c.salesRepEmployeeNumber = e.employeeNumber JOIN offices as o ON e.officeCode = o.officeCode WHERE c.country = o.country;
SELECT lastName, firstName FROM employees WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "Bow");
SELECT productName, MAX(MSRP) FROM products;
SELECT COUNT(customerName) FROM customers WHERE country = "UK";

SELECT productline, Count(*) FROM products GROUP BY productline;
SELECT e.lastname, e.firstname, COUNT(*)  FROM employees AS e JOIN customers AS c ON e.employeeNumber = c.salesRepEmployeeNumber GROUP BY c.salesRepEmployeeNumber;
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;